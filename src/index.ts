import "reflect-metadata";
import * as express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { Container } from "typedi";
import * as TypeORM from "typeorm";
import * as bodyParser from "body-parser";
import * as entities from "./entities";

const PORT = process.env.PORT || "4000";
TypeORM.useContainer(Container)

async function start() {
  await TypeORM.createConnection({
    type: "postgres",
    database: "freeda",
    username: "postgres",
    password: "root",
    port: parseInt("5433"),
    host: "localhost",
    entities: Object.values(entities),
    synchronize: true,
    logger: "advanced-console",
    logging: "all",
    cache: true,
    ssl: false
  });

  const server = express();

  const schema = await buildSchema({
    resolvers: [__dirname + "/resolvers/*"],
    container: Container,
    authChecker: () => true // TODO: Implement an auth checker function
  });

  const apolloServer = new ApolloServer({
    schema,
    playground: true,
    context: async({ req }) => {
      // const user = await getUser(reg);
      const user = { id: 0, name: "Dummy user" }
      return { user }
    }
  });

  server.use(bodyParser.json());

  apolloServer.applyMiddleware({
    app: server,
    path: "/graphql"
  });
  
  server.listen({ port: PORT });
  console.log(`Server running at: ${PORT}`)
}

start();
